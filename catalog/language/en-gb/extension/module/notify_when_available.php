<?php
// Heading
$_['text_out_of_stock']  = "Out of Stock";
$_['error_name_notify']     = 'Name must be between 1 and 32 characters!';
$_['error_email_notify']    = 'E-Mail Address does not appear to be valid!';
$_['error_warning_notify'] = "Please referesh the page and try again.";
$_['text_success_notify']       = 'Success: Your request has been successfully added.';

$_['entry_name_notify'] 	= "Name";
$_['entry_email_notify'] 	= "Email";
$_['entry_message_notify'] = "Message";
$_['button_notify_send'] = "Send";

$_['notify_heading'] 		= "Notify when available";
$_['text_notify_button'] 	= "Notify";

