<?php
 

 $_['text_model_asc'] = 'Моделі від А до Я';
 $_['text_model_desc'] = 'Моделі від Я до А';
 
 $_['text_limit'] = 'На сторінці:';
 $_['text_sort'] = 'Сортувати за:';
 
 $_['text_viewed'] = 'Переглядів:';
 $_['text_further'] = '&rarr;';
 $_['text_january'] = "січень";
 $_['text_february'] = "лютий";
 $_['text_march'] = "березня";
 $_['text_april'] = "квітня";
 $_['text_may'] = "травень";
 $_['text_june'] = "червень";
 $_['text_july'] = "липня";
 $_['text_august'] = "серпня";
 $_['text_september'] = "вересня";
 $_['text_october'] = "жовтня";
 $_['text_november'] = "листопада";
 $_['text_december'] = "грудень";
 $_['text_today'] = "Сьогодні";
 $_['text_date'] = "d M Y";
 $_['text_hours'] = "H:i:s";
 $_['text_edit'] = "Редагувати";
 $_['text_blog'] = "Категорія:";
 $_['text_category'] = "Категорія товару:";
 $_['text_category_record'] = 'Категорія:';
 $_['text_record'] = "Запис:";
 $_['text_product'] = "Товар:";
 $_['text_manufacturer'] = 'Виробник:';
 $_['text_karma'] = "Корисність:";
 $_['text_buy'] = '<span class="seocmspro_buy">Купував на сайті.</span>';
 $_['text_buyproduct'] = '<span class="seocmspro_buy">Купив цей товар.</span>';
 $_['text_registered'] = '<span class="seocmspro_buy">Зареєстровано.</span>';
 $_['text_admin'] = '<span class="seocmspro_buy">Адміністратор.</span>';
 $_['text_buy_ghost'] = '<span class="seocmspro_buy">Гість.</span>';
 $_['text_author'] = 'Автор:';

 $_['error_addfields_name'] = "Неправильне ім'я додаткового поля";
 $_['button_continue'] = "Далі";
 $_['entry_ans'] = 'Ваша відповідь:';
 $_['entry_rating'] = 'Оцініть публікацію: ';
 $_['entry_rating_review'] = 'Оцініть: ';
 $_['entry_minus'] = 'Жахливо';
 $_['entry_bad'] = 'Погано';
 $_['entry_normal'] = 'Задовільно';
 $_['entry_good'] = 'Добре';
 $_['entry_exelent'] = 'Відмінно';
$_['entry_records_more']	= 'Показать еще ';
$_['entry_records_more_end']= '...';

$_['separator_center'] 		= '|';
$_['separator_next'] 		= '&rarr;';
$_['separator_previus'] 	= '&larr;';

if (!isset($_['text_separator'])) {
	$_['text_separator']        = ' &raquo; ';
}

