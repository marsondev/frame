<?php
// Buttons
$_['button_go_to_checkout'] = 'Оформлення замовлення';
$_['button_go_back'] = 'Продовжити покупки';
$_['button_save_cart'] = 'Зберегти кошик';
$_['buttom_save_cart_to_email'] = 'Надіслати на Email';
$_['buttom_save_cart_to_wishlist'] = 'Зберегти до Обраного';
$_['button_cart_add'] = 'Додати до кошика';
$_['button_carousel_prev'] = '&#8249; Перед';
$_['button_carousel_next'] = 'Слід &#8250;';
$_['button_send_cart'] = 'Надіслати';

// Collumns
$_['column_remove'] = 'Видалити';
$_['column_name'] = 'Назва товару';
$_['column_model'] = 'Артикул';
$_['column_image'] = 'Фото';
$_['column_quantity'] = 'Кількість';
$_['column_price'] = 'Ціна';
$_['column_total'] = 'Сума';

// Text
$_['text_to_close'] = 'Натисніть, щоб закрити вікно';
$_['text_total_bottom'] = 'Сума:';
$_['text_tax'] = 'ПДВ: ';
$_['text_model'] = 'Артикул:';
$_['text_ean'] = 'EAN:';
$_['text_jan'] = 'JAN:';
$_['text_isbn'] = 'ISBN:';
$_['text_mpn'] = 'MPN:';
$_['text_location'] = 'Розміщення:';
$_['text_cart_saved'] = 'Кошик збережений: %s';
$_['text_saved_cart'] = 'Кошик збережено: ';
$_['text_instock'] = 'В наявності';
$_['text_availability'] = 'Наявність: ';
$_['text_points'] = 'Бонуси:';
$_['text_cart_weight'] = 'Вага:';
$_['text_coupon_title'] = 'Використовувати код купона';
$_['text_voucher_title'] = 'Використовувати подарунковий сертифікат';
$_['text_shipping_title'] = 'Розрахунок вартості доставки';
$_['text_reward_title_heading'] = 'Використовувати бонуси (Доступно %s)';
$_['text_help_heading'] = 'Виберіть, якщо у вас є код знижки або бали, ви хочете їх використовувати або хотіли б розрахувати вартість доставки.';
$_['text_shipping_help'] = 'Введіть пункт призначення, щоб отримати очікувану вартість доставки.';
$_['text_smca_shipping_method'] = 'Будь ласка, виберіть потрібний спосіб доставки для використання в цьому замовленні.';

// Entry
$_['entry_coupon'] = 'Введіть код купона тут';
$_['entry_voucher'] = 'Введіть подарунковий сертифікат тут';
$_['entry_reward'] = 'Бонуси для використання (макс %s)';
$_['entry_country'] = '-- Виберіть країну --';
$_['entry_zone'] = '-- Виберіть регіон --';
$_['entry_postcode'] = 'Введіть індекс';

// Success
$_['text_success_send'] = 'Успішно: Операція завершена успішно!';
$_['text_success_coupon'] = 'Успішно: Ваш купон застосовано!';
$_['text_success_voucher'] = 'Успішно: Ваш подарунковий сертифікат застосовано!';
$_['text_success_shipping'] = 'Успішно: Вартість доставки розрахована!';
$_['text_success_reward'] = 'Успішно: Знижка за бонусними балами зарахована!';

// Error
$_['error_email_send'] = 'E-Mail введено неправильно!';
$_['error_stock'] = 'Товарів виділених червоним немає в необхідній кількості!';
$_['error_smca_coupon_empty'] = 'Введіть код купона!';
$_['error_smca_coupon'] = 'Купон недійсний або закінчився термін його дії!';
$_['error_smca_voucher_empty'] = 'Введіть код сертифіката!';
$_['error_smca_voucher'] = 'Подарунковий сертифікат недійсний або його баланс вже використаний!';
$_['error_smca_shipping'] = 'Виберіть спосіб доставки!';
$_['error_smca_postcode'] = 'Поштовий індекс повинен містити від 2 до 10 символів!';
$_['error_smca_country'] = 'Виберіть країну!';
$_['error_smca_zone'] = 'Виберіть регіон!';
$_['error_smca_no_shipping'] = 'Немає варіантів доставки. Будь ласка, <a href="%s" target="blank">напишіть нам</a> для вирішення проблеми!';
$_['error_smca_reward'] = 'Введіть кількість бонусних балів для використання!';
$_['error_smca_points'] = 'Вам бракує %s бонусних балів!';
$_['error_smca_maximum'] = 'Можна використовувати максимально %s балів!';
?>
