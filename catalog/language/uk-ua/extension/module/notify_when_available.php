<?php
// Heading
$_['text_out_of_stock']  = "Немає в наявності";
$_['error_name_notify']     = 'Имя должно содержать 1-32 символов!';
$_['error_email_notify']    = "Ім'я має містити 1-32 символи!";
$_['error_warning_notify'] = "Оновіть сторінку та спробуйте ще раз.";
$_['text_success_notify']       = 'Заявка успішно надіслана';

$_['entry_name_notify'] 	= "Ім'я";
$_['entry_email_notify'] 	= "Email";
$_['entry_message_notify'] = "Коментар";
$_['button_notify_send'] = "Відправити";

$_['notify_heading'] 		= "Повідомити про наявність";
$_['text_notify_button'] 	= "Повідомити";

