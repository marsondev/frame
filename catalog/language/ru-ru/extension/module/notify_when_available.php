<?php
// Heading
$_['text_out_of_stock']  = "Нет в наличии";
$_['error_name_notify']     = 'Имя должно содержать 1-32 символов!';
$_['error_email_notify']    = 'E-Mail не правильный!';
$_['error_warning_notify'] = "Обновите страницу и попробуйте выполнить еще раз.";
$_['text_success_notify']       = 'Заявка успешно отправлена';

$_['entry_name_notify'] 	= "Имя";
$_['entry_email_notify'] 	= "Email";
$_['entry_message_notify'] = "Комментарий";
$_['button_notify_send'] = "Отправить";

$_['notify_heading'] 		= "Сообщить о наличии";
$_['text_notify_button'] 	= "Сообщить";

