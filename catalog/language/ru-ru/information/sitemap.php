<?php

// Heading
$_['heading_title']    = 'Карта сайта';

// Text
$_['text_special']     = 'Акции';
$_['text_account']     = 'Личный кабинет';
$_['text_edit']        = 'Личная информация';
$_['text_password']    = 'Пароль';
$_['text_address']     = 'Мои адреса';
$_['text_history']     = 'История заказа';
$_['text_download']    = 'Файлы для скачивания';
$_['text_cart']        = 'Корзина покупок';
$_['text_checkout']    = 'Оформление заказа';
$_['text_search']      = 'Поиск';
$_['text_information'] = 'Информация';
$_['text_contact']     = 'Связаться с нами';
$_['text_blog_category'] = 'Категории статей';
$_['text_articles']	   = 'Статьи';
$_['text_products']	   = 'Товары';
$_['text_categories']  = 'Категории';
$_['text_manufacturers'] = 'Производители';