<?php

class ControllerExtensionFeedGoogleSitemap extends Controller {
	public function index() {
        if ($this->config->get('feed_google_sitemap_status')) {
            $product_limit = 100;
            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            if (isset($this->request->get['type'])) {

                $this->load->model('localisation/language');

                $languages = $this->model_localisation_language->getLanguages();

                $real_language = $this->session->data['language'];

                if ($this->request->get['type'] == 'product') {
                    $output = '<?xml version="1.0" encoding="UTF-8"?>';
                    $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

                    if(isset($this->request->get['page_number'])){
                        $filter_data = array(
                            'start' => (int)$this->request->get['page_number']*$product_limit,
                            'limit' => $product_limit,
                        );

                        foreach ($languages as $language) {
                            $products = $this->model_catalog_product->getProducts($filter_data);

                            foreach ($products as $product) {
                                $correct_link = $this->getCorrectLink($this->url->link('product/product', 'product_id=' . $product['product_id']), $language['code']);
                                if ($product['image']) {
                                    $output .= '<url>';
                                    $output .= '  <loc>' . $correct_link . '</loc>';
                                    $output .= '  <changefreq>weekly</changefreq>';
                                    $output .= '  <lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
                                    $output .= '  <priority>0.8</priority>';
                                    $output .= '  <image:image>';
                                    $output .= '  <image:loc>' . $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')) . '</image:loc>';
                                    $output .= '  <image:caption>' . $product['name'] . '</image:caption>';
                                    $output .= '  <image:title>' . $product['name'] . '</image:title>';
                                    $output .= '  </image:image>';
                                    $output .= '</url>';
                                }
                            }
                        }
                    }

                    $output .= '</urlset>';
                } elseif ($this->request->get['type'] == 'category') {
                    $output = '<?xml version="1.0" encoding="UTF-8"?>';
                    $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

                    $this->load->model('catalog/category');

                    foreach($languages as $language) {
                        $output .= $this->getCategories(0, '', $language['code']);
                    }

                    $output .= '</urlset>';
                } elseif ($this->request->get['type'] == 'info') {
                    $output = '<?xml version="1.0" encoding="UTF-8"?>';
                    $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

                    $this->load->model('catalog/information');

                    foreach($languages as $language) {

                        $informations = $this->model_catalog_information->getInformations();

                        foreach ($informations as $information) {
                            $correct_link = $this->getCorrectLink($this->url->link('information/information', 'information_id=' . $information['information_id']), $language['code']);

                            $output .= '<url>';
                            $output .= '  <loc>' . $correct_link . '</loc>';
                            $output .= '  <changefreq>weekly</changefreq>';
                            $output .= '  <priority>0.5</priority>';
                            $output .= '</url>';
                        }
                    }

                    $output .= '</urlset>';
                } elseif ($this->request->get['type'] == 'filter') {
                    $output = '<?xml version="1.0" encoding="UTF-8"?>';
                    $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

                    $this->load->model('extension/module/ocfilter');

                    foreach($languages as $language) {
                        $pages = $this->model_extension_module_ocfilter->getAllPages();

                        foreach ($pages as $page) {
                            $correct_link = $this->getCorrectLink($this->url->link('product/category', 'path=' . $page['category_id']) . $page['keyword'], $language['code']).'/';

                            $output .= '<url>';
                            $output .= '  <loc>' . $correct_link . '</loc>';
                            $output .= '  <changefreq>weekly</changefreq>';
                            $output .= '  <priority>0.9</priority>';
                            $output .= '</url>';
                        }
                    }
                    $output .= '</urlset>';
                }

                $this->session->data['language'] = $real_language;

                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
            } else {
                $output = '<?xml version="1.0" encoding="UTF-8"?>';
				$output .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

                $total_products = (int)$this->model_catalog_product->getTotalProducts();

                if($total_products/$product_limit >= 1){
	                for($i = 1; $i < ceil($total_products/$product_limit); $i++){
	                    $output .= '<sitemap>';
	                    $output .= '  <loc>' . $this->url->link('extension/feed/google_sitemap/product', 'page_number='.$i) . '</loc>';
	                    $output .= '</sitemap>';
	                }
	            }else{
                    $output .= '<sitemap>';
                    $output .= '  <loc>' . $this->url->link('extension/feed/google_sitemap/product', 'page_number=1') . '</loc>';
                    $output .= '</sitemap>';
	            }

                $output .= '<sitemap>';
                $output .= '  <loc>' . $this->url->link('extension/feed/google_sitemap/category') . '</loc>';
                $output .= '</sitemap>';

                $output .= '<sitemap>';
                $output .= '  <loc>' . $this->url->link('extension/feed/google_sitemap/info') . '</loc>';
                $output .= '</sitemap>';

                $this->load->model('extension/module/ocfilter');

                if(count($this->model_extension_module_ocfilter->getAllPages()) > 0){
	                $output .= '<sitemap>';
	                $output .= '  <loc>' . $this->url->link('extension/feed/google_sitemap/filter') . '</loc>';
	                $output .= '</sitemap>';
	            }

                $output .= '</sitemapindex>';

                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
            }
        }
	}

	protected function getCategories($parent_id, $current_path = '', $code) {
	    $this->load->model('catalog/product');

		$output = '';

		$results = $this->model_catalog_category->getCategories($parent_id);

		foreach ($results as $result) {

            if (!$current_path) {
				$new_path = $result['category_id'];
			} else {
				$new_path = $current_path . '_' . $result['category_id'];
			}

            $correct_link = $this->getCorrectLink($this->url->link('product/category', 'path=' . $new_path), $code);

			$output .= '<url>';
			$output .= '  <loc>' . $correct_link . '</loc>';
			$output .= '  <changefreq>weekly</changefreq>';
			$output .= '  <priority>0.9</priority>';
			$output .= '</url>';

			$output .= $this->getCategories($result['category_id'], $new_path, $code);
		}

		return $output;
	}

	public function getCorrectLink($link, $code){
        $new_link = $link;
        switch ($code){
            case 'ru-ru':
                $change_link = false;
                break;
            case 'uk-ua':
                $change_link = 'ua/';
                break;
            case 'en-gb':
                $change_link = 'en/';
                break;
        }

        if($change_link){
            $new_link = str_replace(HTTPS_SERVER, HTTPS_SERVER.$change_link, $new_link);
        }

        return $new_link;
    }

    public function product(){
        $product_limit = 100;
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $real_language = $this->session->data['language'];

        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        if(isset($this->request->get['page_number'])){
            $filter_data = array(
                'start' => (int)($this->request->get['page_number']-1)*$product_limit,
                'limit' => $product_limit,
            );

            foreach ($languages as $language) {
                $products = $this->model_catalog_product->getProducts($filter_data);

                foreach ($products as $product) {
                    $correct_link = $this->getCorrectLink($this->url->link('product/product', 'product_id=' . $product['product_id']), $language['code']);
                    if ($product['image']) {
                        $output .= '<url>';
                        $output .= '  <loc>' . $correct_link . '</loc>';
                        $output .= '  <changefreq>weekly</changefreq>';
                        $output .= '  <lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
                        $output .= '  <priority>0.8</priority>';
                        $output .= '  <image:image>';
                        $output .= '  <image:loc>' . $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')) . '</image:loc>';
                        $output .= '  <image:caption>' . $product['name'] . '</image:caption>';
                        $output .= '  <image:title>' . $product['name'] . '</image:title>';
                        $output .= '  </image:image>';
                        $output .= '</url>';
                    }
                }
            }
        }

        $output .= '</urlset>';

        $this->session->data['language'] = $real_language;

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }

    public function category(){
        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $real_language = $this->session->data['language'];

        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        $this->load->model('catalog/category');

        foreach($languages as $language) {
            $output .= $this->getCategories(0, '', $language['code']);
        }

        $output .= '</urlset>';

        $this->session->data['language'] = $real_language;

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }

    public function info(){
        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        $this->load->model('catalog/information');

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $real_language = $this->session->data['language'];

        foreach($languages as $language) {

            $informations = $this->model_catalog_information->getInformations();

            foreach ($informations as $information) {
                $correct_link = $this->getCorrectLink($this->url->link('information/information', 'information_id=' . $information['information_id']), $language['code']);

                $output .= '<url>';
                $output .= '  <loc>' . $correct_link . '</loc>';
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <priority>0.5</priority>';
                $output .= '</url>';
            }
        }

        $output .= '</urlset>';
        
        $this->session->data['language'] = $real_language;

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }

    public function filter(){
        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $real_language = $this->session->data['language'];

        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        $this->load->model('extension/module/ocfilter');

        foreach($languages as $language) {
            $pages = $this->model_extension_module_ocfilter->getAllPages();

            foreach ($pages as $page) {
                $correct_link = $this->getCorrectLink($this->url->link('product/category', 'path=' . $page['category_id']) . $page['keyword'], $language['code']).'/';

                $output .= '<url>';
                $output .= '  <loc>' . $correct_link . '</loc>';
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <priority>0.9</priority>';
                $output .= '</url>';
            }
        }
        $output .= '</urlset>';
        
        $this->session->data['language'] = $real_language;

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }
}

/*class ControllerExtensionFeedGoogleSitemap extends Controller {
	public function index() {
		if ($this->config->get('feed_google_sitemap_status')) {
			$output  = '<?xml version="1.0" encoding="UTF-8"?>';
			$output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

			$this->load->model('catalog/product');
			$this->load->model('tool/image');

			$products = $this->model_catalog_product->getProducts();

			foreach ($products as $product) {
				if ($product['image']) {
					$output .= '<url>';
					$output .= '  <loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '</loc>';
					$output .= '  <changefreq>weekly</changefreq>';
					$output .= '  <lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
					$output .= '  <priority>1.0</priority>';
					$output .= '  <image:image>';
					$output .= '  <image:loc>' . $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')) . '</image:loc>';
					$output .= '  <image:caption>' . $product['name'] . '</image:caption>';
					$output .= '  <image:title>' . $product['name'] . '</image:title>';
					$output .= '  </image:image>';
					$output .= '</url>';
				}
			}

			$this->load->model('catalog/category');

			$output .= $this->getCategories(0);

			$this->load->model('catalog/manufacturer');

			$manufacturers = $this->model_catalog_manufacturer->getManufacturers();

			foreach ($manufacturers as $manufacturer) {
				$output .= '<url>';
				$output .= '  <loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']) . '</loc>';
				$output .= '  <changefreq>weekly</changefreq>';
				$output .= '  <priority>0.7</priority>';
				$output .= '</url>';

				$products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));

				foreach ($products as $product) {
					$output .= '<url>';
					$output .= '  <loc>' . $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id']) . '</loc>';
					$output .= '  <changefreq>weekly</changefreq>';
					$output .= '  <priority>1.0</priority>';
					$output .= '</url>';
				}
			}

			$this->load->model('catalog/information');

			$informations = $this->model_catalog_information->getInformations();

			foreach ($informations as $information) {
				$output .= '<url>';
				$output .= '  <loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id']) . '</loc>';
				$output .= '  <changefreq>weekly</changefreq>';
				$output .= '  <priority>0.5</priority>';
				$output .= '</url>';
			}

			$output .= '</urlset>';

			$this->response->addHeader('Content-Type: application/xml');
			$this->response->setOutput($output);
		}
	}

	protected function getCategories($parent_id, $current_path = '') {
		$output = '';

		$results = $this->model_catalog_category->getCategories($parent_id);

		foreach ($results as $result) {
			if (!$current_path) {
				$new_path = $result['category_id'];
			} else {
				$new_path = $current_path . '_' . $result['category_id'];
			}

			$output .= '<url>';
			$output .= '  <loc>' . $this->url->link('product/category', 'path=' . $new_path) . '</loc>';
			$output .= '  <changefreq>weekly</changefreq>';
			$output .= '  <priority>0.7</priority>';
			$output .= '</url>';

			$products = $this->model_catalog_product->getProducts(array('filter_category_id' => $result['category_id']));

			foreach ($products as $product) {
				$output .= '<url>';
				$output .= '  <loc>' . $this->url->link('product/product', 'path=' . $new_path . '&product_id=' . $product['product_id']) . '</loc>';
				$output .= '  <changefreq>weekly</changefreq>';
				$output .= '  <priority>1.0</priority>';
				$output .= '</url>';
			}

			$output .= $this->getCategories($result['category_id'], $new_path);
		}

		return $output;
	}
}*/
