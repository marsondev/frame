<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {

        $this->load->language('extension/module/notify_when_available');
        $data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
        $data['out_of_stock_show'] = $this->config->get('module_notify_when_available_stock');
        $data['out_of_stock_notify'] = $this->config->get('module_notify_when_available_notify');
        
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'], 'product_in_cart')
				);
			}
		}

		$data['module'] = $module++;

		return $this->load->view('extension/module/carousel', $data);
	}
}