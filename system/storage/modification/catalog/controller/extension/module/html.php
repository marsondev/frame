<?php
class ControllerExtensionModuleHTML extends Controller {
	public function index($setting) {

        $this->load->language('extension/module/notify_when_available');
        $data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
        $data['out_of_stock_show'] = $this->config->get('module_notify_when_available_stock');
        $data['out_of_stock_notify'] = $this->config->get('module_notify_when_available_notify');
        
		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');

			return $this->load->view('extension/module/html', $data);
		}
	}
}