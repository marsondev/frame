<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonHeader extends Controller {
	public function index() {

        
            $data['oc_settings'] = $this->config->get('oneclick_settings');
            $store_id = $this->config->get('config_store_id');
            $customer_group_id = ($this->customer->isLogged())?$this->customer->getGroupId():$this->config->get('config_customer_group_id');
            if((isset($data['oc_settings']['stores']) && in_array($store_id, $data['oc_settings']['stores'])) || (isset($data['oc_settings']['customer_groups']) && in_array($customer_group_id, $data['oc_settings']['customer_groups']))){
                $data['oc_settings']['status'] = false;
            }
            if($data['oc_settings']['status']) {
                $this->document->addScript('catalog/view/javascript/oneclick/jquery.magnific-popup.new.min.js');
                $this->document->addScript('catalog/view/javascript/oneclick/script.js');
                $this->document->addStyle('catalog/view/javascript/oneclick/magnific-popup.new.css');
                $this->document->addStyle('catalog/view/theme/default/stylesheet/oneclick.css');
            }
        
            

        $data['smca_form_data']         = (array)$this->config->get('ocdev_smart_cart_form_data');
        $data['smca_store_id']          = (int)$this->config->get('config_store_id');
        $data['smca_customer_group_id'] = ($this->customer->isLogged()) ? (int)$this->customer->getGroupId() : (int)$this->config->get('config_customer_group_id');
      
$data['d_ajax_search'] = $this->load->controller('extension/module/d_ajax_search');
		// Analytics
		$this->load->model('setting/extension');

		$data['analytics'] = array();

		$analytics = $this->model_setting_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

					if (is_array($this->document->getOpengraph())) { 
	$data['opengraphs'] = $this->document->getOpengraph();
	}
				

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();

		if(isset($this->request->get['page']) && $this->request->get['page'] == 2){
		    foreach($data['links'] as $key => $link){
		        if($link['rel'] == 'prev'){
                    $is_prev = true;
                }
            }
        }

		if(isset($this->request->get['page']) && $this->request->get['page'] == 2){
		    foreach($data['links'] as $key => $link){
		        if($link['rel'] == 'canonical'){
		        	if(!isset($is_prev)){
	                    $data['links'][] = array(
	                        'href' => $key,
	                        'rel'  => "prev"
	                    );
		        	}
                }
            }
        }

		$data['robots'] = $this->document->getRobots();
		$data['styles'] = $this->document->getStyles();

    // OCFilter start
    $data['noindex'] = $this->document->isNoindex();
    // OCFilter end
      
		$data['scripts'] = $this->document->getScripts('header');
		$data['lang'] = $this->language->get('code');

		if($data['lang'] == 'ua'){
			$data['lang'] = 'uk';
		}
		
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');
$data['store_url'] = HTTPS_SERVER;
		
		
		$host = isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')) ? HTTPS_SERVER : HTTP_SERVER;
		if ($this->request->server['REQUEST_URI'] == '/') {
			$data['og_url'] = $this->url->link('common/home');
		} else {
			$data['og_url'] = $host . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		}
		
		$data['og_image'] = $this->document->getOgImage();

		if(isset($this->request->get['product_id'])){
			$this->load->model('catalog/product');

			$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
			
			if(count($product_info) > 0 && $product_info['image']){
				$data['og_image'] = HTTPS_SERVER . 'image/' . $product_info['image'];
			}
		}
		
		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));
		
		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['currency'] = $this->load->controller('common/currency');
		if ($this->config->get('configblog_blog_menu')) {
			$data['blog_menu'] = $this->load->controller('blog/menu');
		} else {
			$data['blog_menu'] = '';
		}
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
		$data['menu'] = $this->load->controller('common/menu');

        $data['shop_name'] = $this->config->get('config_name');
        $data['home_url'] = HTTPS_SERVER;
        $data['address'] = nl2br($this->config->get('config_address'));
		$data['telephone'] = $this->config->get('config_telephone');


            $this->load->language('extension/module/notify_when_available');
            $data['notify_heading'] = $this->language->get('notify_heading');
            $data['entry_name_notify']     = $this->language->get('entry_name_notify');
            $data['entry_email_notify']    = $this->language->get('entry_email_notify');
            $data['entry_message_notify']      = $this->language->get('entry_message_notify');
            $data['button_notify_send']  = $this->language->get('button_notify_send');
            if ($this->customer->isLogged()) {
                $data['notify_name']           = $this->customer->getFirstName().' '.$this->customer->getLastName();
                $data['notify_email']          = $this->customer->getEmail();
            }else{
                $data['notify_name']           = '';
                $data['notify_email']          = '';
            }
            $data['out_of_stock_show'] = $this->config->get('module_notify_when_available_stock');
            $data['out_of_stock_notify'] = $this->config->get('module_notify_when_available_notify');
       
		return $this->load->view('common/header', $data);
	}
}
