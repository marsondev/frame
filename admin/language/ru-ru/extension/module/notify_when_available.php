<?php
// Heading
$_['page_title']    = "Сообщить о наличии";
$_['text_extension']   = 'Расширения';
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля сохранено!';
$_['text_edit']        = 'Редактирование модуля';
$_['text_unregister']  	= 'Гость';
$_['text_register']  		= 'Клиент';
$_['text_show_customer'] 	= 'Подробнее';
$_['text_store_name'] 		= "Магазин: %s";

// Entry
$_['entry_status']     		= 'Статус';
$_['entry_out_of_stock']  	= 'Показывать "Нет в наличии"';
$_['entry_notify_button']  	= 'Показывать кнопку "Сообщить"';
$_['entry_email_subject'] 	= 'Заголовок письма:';
$_['entry_email_body'] 		= 'Текст письма:';

$_['tab_notify_product']    = 'Подписки';
$_['tab_email']     		= 'Шаблон письма';
$_['tab_general'] 			= 'Настройки модуля';
$_['column_customer']  		= 'Клиент';
$_['column_product']  		= 'Товар';
$_['column_message']  		= 'Комментарий';
$_['column_date_added']  	= 'Дата';

// Error
$_['error_permission'] 		= 'Предупреждение: у вас нет прав на редактирование модуля!';
$_['error_email_subject'] 	= 'Заполните поле.';
$_['error_email_body'] 		= 'Заполните поле.';

// email title
$_['heading_email_to_customer']  	= 'Для клиента';
$_['heading_email_to_admin']  		= 'Для админа';

// help
$_['help_email_subject'] 	= "Заголовок для email-сообщения.";
$_['help_email_body'] 		= "Содержание сообщения. Пожалуйста, используйте приведенные ниже шорткоды для динамических значени1";

$_['email_subject'] 		= "Товар уже в наличии!";
$_['email_body'] 			= "<table style='width:100%'>  <tbody><tr> <td align='center'> <table style='width:100%;margin:0 auto;border:1px solid #f0f0f0;padding:10px;line-height:1.8'>  <tbody><tr><td><p>Здравствуйте, <strong>{name}</strong>.</p><p>Товар <a href='{product_url}' target='_blank'>{product_name}</a> уже появился в наличии. Чтобы заказать товар, перейдите по ссылке: {product_url}</p><p>Спасибо за интерес к товарам нашего магазина!</p></td></tr></tbody></table> </td></tr>  </tbody> </table>";																																																					
																																																$_['heading_title']    = '<a href="https://opencart3x.ru" target="_blank" title="Разработчик Opencart3x.ru" style="color:#233746"><i class="fa fa-circle-o"></i></a> '. $_['page_title'];